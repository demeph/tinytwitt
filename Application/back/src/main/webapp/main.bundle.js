webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<sidebar></sidebar>\r\n<!-- Content -->\r\n<div>\r\n    <div class=\"row\">\r\n        <div class=\"col-md-12 col-sm-12\">\r\n            <router-outlet></router-outlet>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<app-footer></app-footer>\r\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return adressBackEnd; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var adressBackEnd = 'https://1-dot-webcloud-122127.appspot.com/_ah/api/';
var AppComponent = (function () {
    function AppComponent(router) {
        var _this = this;
        this.router = router;
        this.router.events.subscribe(function (vat) {
            _this.loading = localStorage.getItem('user');
        }, function (err) { return console.log(err); });
    }
    AppComponent.prototype.ngOnInit = function () {
    };
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'my-app',
            template: __webpack_require__("../../../../../src/app/app.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_header_header_component__ = __webpack_require__("../../../../../src/app/components/header/header.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_nav_sidebar_component__ = __webpack_require__("../../../../../src/app/components/nav/sidebar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_footer_footer_component__ = __webpack_require__("../../../../../src/app/components/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__guards_authentication_guard__ = __webpack_require__("../../../../../src/app/guards/authentication.guard.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_Authentication_authentication_services__ = __webpack_require__("../../../../../src/app/services/Authentication/authentication.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__app_routing__ = __webpack_require__("../../../../../src/app/app.routing.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__services_User_user_services__ = __webpack_require__("../../../../../src/app/services/User/user.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__components_CalculFollowers_calculFollowers_compotent__ = __webpack_require__("../../../../../src/app/components/CalculFollowers/calculFollowers.compotent.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_Twitts_twitts_service__ = __webpack_require__("../../../../../src/app/services/Twitts/twitts.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16_ng_gapi__ = __webpack_require__("../../../../ng-gapi/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__components_userstofofollow_userstofollow_component__ = __webpack_require__("../../../../../src/app/components/userstofofollow/userstofollow.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__components_testMessage_testMessage_component__ = __webpack_require__("../../../../../src/app/components/testMessage/testMessage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__components_readme_readme_component__ = __webpack_require__("../../../../../src/app/components/readme/readme.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





















var gapiClientConfig = {
    client_id: "webcloud-122127",
    discoveryDocs: ["https://analyticsreporting.googleapis.com/$discovery/rest?version=v4"],
    scope: [
        "https://www.googleapis.com/auth/analytics.readonly",
        "https://www.googleapis.com/auth/analytics"
    ].join(" ")
};
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_10__app_routing__["a" /* routing */],
                __WEBPACK_IMPORTED_MODULE_15__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_16_ng_gapi__["a" /* GoogleApiModule */].forRoot({
                    provide: __WEBPACK_IMPORTED_MODULE_16_ng_gapi__["c" /* NG_GAPI_CONFIG */],
                    useValue: gapiClientConfig
                }),
            ],
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_4__components_header_header_component__["a" /* HeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_5__components_nav_sidebar_component__["a" /* SidebarComponent */],
                __WEBPACK_IMPORTED_MODULE_6__components_footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_7__components_dashboard_dashboard_component__["a" /* DashboardComponent */],
                __WEBPACK_IMPORTED_MODULE_13__components_CalculFollowers_calculFollowers_compotent__["a" /* CalculFollowersComponent */],
                __WEBPACK_IMPORTED_MODULE_17__components_userstofofollow_userstofollow_component__["a" /* usersToFollowComponent */],
                __WEBPACK_IMPORTED_MODULE_19__components_testMessage_testMessage_component__["a" /* testComponent */],
                __WEBPACK_IMPORTED_MODULE_20__components_readme_readme_component__["a" /* ReadmeComponent */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]],
            providers: [
                __WEBPACK_IMPORTED_MODULE_8__guards_authentication_guard__["a" /* AuthenticationGuard */],
                __WEBPACK_IMPORTED_MODULE_9__services_Authentication_authentication_services__["a" /* AuthenticationService */],
                __WEBPACK_IMPORTED_MODULE_12__services_User_user_services__["a" /* UserService */],
                __WEBPACK_IMPORTED_MODULE_14__services_Twitts_twitts_service__["a" /* TwittsService */],
                __WEBPACK_IMPORTED_MODULE_18__angular_common__["d" /* DatePipe */],
            ],
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/app.routing.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return routing; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_dashboard_dashboard_component__ = __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_CalculFollowers_calculFollowers_compotent__ = __webpack_require__("../../../../../src/app/components/CalculFollowers/calculFollowers.compotent.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_userstofofollow_userstofollow_component__ = __webpack_require__("../../../../../src/app/components/userstofofollow/userstofollow.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_testMessage_testMessage_component__ = __webpack_require__("../../../../../src/app/components/testMessage/testMessage.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_readme_readme_component__ = __webpack_require__("../../../../../src/app/components/readme/readme.component.ts");






var appRoutes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_1__components_dashboard_dashboard_component__["a" /* DashboardComponent */] },
    { path: 'calcul1', component: __WEBPACK_IMPORTED_MODULE_2__components_CalculFollowers_calculFollowers_compotent__["a" /* CalculFollowersComponent */] },
    { path: 'readme', component: __WEBPACK_IMPORTED_MODULE_5__components_readme_readme_component__["a" /* ReadmeComponent */] },
    { path: 'userstofollow', component: __WEBPACK_IMPORTED_MODULE_3__components_userstofofollow_userstofollow_component__["a" /* usersToFollowComponent */] },
    { path: 'testMessages', component: __WEBPACK_IMPORTED_MODULE_4__components_testMessage_testMessage_component__["a" /* testComponent */] },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];
var routing = __WEBPACK_IMPORTED_MODULE_0__angular_router__["b" /* RouterModule */].forRoot(appRoutes, { useHash: true });


/***/ }),

/***/ "../../../../../src/app/components/CalculFollowers/calculFollowers.compotent.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Mesure; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CalculFollowersComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_Twitts_twitts_service__ = __webpack_require__("../../../../../src/app/services/Twitts/twitts.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_Message_message_model__ = __webpack_require__("../../../../../src/app/models/Message/message.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__interfaces_calculMath_calculMath__ = __webpack_require__("../../../../../src/app/interfaces/calculMath/calculMath.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Mesure = (function () {
    function Mesure(ind, tps) {
        this.index = ind;
        this.tps = tps;
    }
    return Mesure;
}());

var CalculFollowersComponent = (function () {
    function CalculFollowersComponent(messageS) {
        this.messageS = messageS;
        this.title = "Calcul en fonction des followers";
        this.tabMesure100 = [];
        this.tabMesure1000 = [];
        this.tabMesure2500 = [];
        this.bool1 = false;
        this.bool2 = false;
        this.bool3 = false;
        this.faitCalcul = false;
    }
    CalculFollowersComponent.prototype.ngOnInit = function () {
        this.bool1 = this.bool2 = this.bool3 = false;
        this.calc1 = this.calc2 = this.calc3 = null;
    };
    CalculFollowersComponent.prototype.faitMoiDeMesure100 = function (nbMesure) {
        var _this = this;
        if (this.faitCalcul) {
            this.tabMesure100 = [];
            var user100 = 4649932115935232;
            console.log(nbMesure);
            var datedeb_1, datefin_1;
            var _loop_1 = function (i) {
                var text = "mesure pour user2500 message " + i.toString();
                var message = new __WEBPACK_IMPORTED_MODULE_2__models_Message_message_model__["a" /* Message */](text, user100);
                console.log(message);
                datedeb_1 = new Date().getTime();
                this_1.messageS.addTwitts(message).subscribe(function (complete) {
                    datefin_1 = new Date().getTime();
                    _this.tabMesure100.push(new Mesure(i, (datefin_1 - datedeb_1)));
                }, function (err) { return console.log(err); });
            };
            var this_1 = this;
            for (var i = 1; i <= nbMesure; i = i + 1) {
                _loop_1(i);
            }
            this.bool1 = true;
            setTimeout(function () {
                _this.calc1 = new __WEBPACK_IMPORTED_MODULE_3__interfaces_calculMath_calculMath__["a" /* calculMath */]();
                _this.calc1.moyenne = _this.calc1.calculAvg(_this.tabMesure100);
                _this.calc1.variance = _this.calc1.calculVar(_this.tabMesure100);
            }, 30000);
        }
    };
    CalculFollowersComponent.prototype.faitMoiDeMesure1000 = function (nbMesure) {
        var _this = this;
        if (this.faitCalcul) {
            this.tabMesure1000 = [];
            var user1000 = 4843829756690432;
            console.log(nbMesure);
            var datedeb_2, datefin_2;
            var _loop_2 = function (i) {
                var text = "mesure pour user2500 message " + i.toString();
                var message = new __WEBPACK_IMPORTED_MODULE_2__models_Message_message_model__["a" /* Message */](text, user1000);
                console.log(message);
                datedeb_2 = new Date().getTime();
                this_2.messageS.addTwitts(message).subscribe(function (complete) {
                    datefin_2 = new Date().getTime();
                    _this.tabMesure1000.push(new Mesure(i, (datefin_2 - datedeb_2)));
                }, function (err) { return console.log(err); });
            };
            var this_2 = this;
            for (var i = 1; i <= nbMesure; i = i + 1) {
                _loop_2(i);
            }
            this.bool2 = true;
            setTimeout(function () {
                _this.calc2 = new __WEBPACK_IMPORTED_MODULE_3__interfaces_calculMath_calculMath__["a" /* calculMath */]();
                _this.calc2.moyenne = _this.calc2.calculAvg(_this.tabMesure1000);
                _this.calc2.variance = _this.calc2.calculVar(_this.tabMesure1000);
            }, 30000);
        }
    };
    CalculFollowersComponent.prototype.faitMoiDeMesure2500 = function (nbMesure) {
        var _this = this;
        if (this.faitCalcul) {
            this.tabMesure2500 = [];
            var user2500 = 5580132374806528;
            console.log(nbMesure);
            var datedeb_3, datefin_3;
            var _loop_3 = function (i) {
                var text = "mesure pour user2500 message " + i.toString();
                var message = new __WEBPACK_IMPORTED_MODULE_2__models_Message_message_model__["a" /* Message */](text, user2500);
                console.log(message);
                datedeb_3 = new Date().getTime();
                this_3.messageS.addTwitts(message).subscribe(function (complete) {
                    datefin_3 = new Date().getTime();
                    _this.tabMesure2500.push(new Mesure(i, (datefin_3 - datedeb_3)));
                }, function (err) { return console.log(err); });
            };
            var this_3 = this;
            for (var i = 1; i <= nbMesure; i = i + 1) {
                _loop_3(i);
            }
            this.bool3 = true;
            setTimeout(function () {
                _this.calc3 = new __WEBPACK_IMPORTED_MODULE_3__interfaces_calculMath_calculMath__["a" /* calculMath */]();
                _this.calc3.moyenne = _this.calc3.calculAvg(_this.tabMesure2500);
                _this.calc3.variance = _this.calc3.calculVar(_this.tabMesure2500);
            }, 30000);
        }
    };
    CalculFollowersComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            template: __webpack_require__("../../../../../src/app/components/CalculFollowers/calculfollowers.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_Twitts_twitts_service__["a" /* TwittsService */]])
    ], CalculFollowersComponent);
    return CalculFollowersComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/CalculFollowers/calculfollowers.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\" style=\"background-color: white\">\r\n  <!-- Content Header (Page header) -->\r\n  <section class=\"content-header\">\r\n    <h1>\r\n      {{title}}\r\n    </h1>\r\n  </section>\r\n\r\n  <section class=\"content\" style=\"background-color: white;margin: 0 auto 0 auto;text-align: center \">\r\n    <div class=\"col-md-12\" style=\"margin-bottom: 5px\">\r\n        Nombre de mesures :<br/>\r\n        <input #val type=\"number\" class=\"form-control\">\r\n        <button (click)=\"faitMoiDeMesure100(val.value)\" style=\"margin:3px 0 0 15px\" class=\"btn btn-danger\">Poster un tweet pour un utilisateur avec 100 follower</button>\r\n        <br/>\r\n        <div *ngIf=\"bool1\">La moyenne et la variance sont calcules dans 25 secondes</div>\r\n        <div style=\"margin: 2px\" *ngIf=\"calc1\">Moyenne : {{calc1.moyenne}}, variance : {{calc1.variance}}</div>\r\n        <div *ngFor=\"let mes of tabMesure100\">\r\n          <span class=\"form-control\">{{mes.index}} : {{mes.tps}}</span>\r\n        </div>\r\n        <hr>\r\n    </div>\r\n    <div class=\"col-md-12\">\r\n        Nombre de mesures :<br/>\r\n        <input #val2 type=\"number\" class=\"form-control\">\r\n        <button (click)=\"faitMoiDeMesure1000(val2.value)\" style=\"margin-left:15px\" class=\"btn btn-danger\">Poster un tweet pour un utilisateur avec 1000 follower</button>\r\n        <br/>\r\n      <div *ngIf=\"bool2\">La moyenne et la variance sont calcules dans 25 secondes</div>      <div style=\"margin: 2px\" *ngIf=\"calc2\">Moyenne : {{calc2.moyenne}}, variance : {{calc2.variance}}</div>\r\n        <div *ngFor=\"let mes of tabMesure1000\">\r\n          <span class=\"form-control\">{{mes.index}} : {{mes.tps}}</span>\r\n        </div>\r\n        <hr>\r\n    </div>\r\n    <div class=\"col-md-12\">\r\n        Nombre de mesures :<br/>\r\n        <input #val3 type=\"number\" class=\"form-control\">\r\n        <button (click)=\"faitMoiDeMesure2500(val3.value)\" style=\"margin-left:15px\" class=\"btn btn-danger\">Poster un tweet pour un utilisateur avec 2500 follower</button>\r\n        <br/>\r\n      <div *ngIf=\"bool3\">La moyenne et la variance sont calcules dans 25 secondes</div>\r\n      <div style=\"margin: 2px\" *ngIf=\"calc3\">Moyenne : {{calc3.moyenne}}, variance : {{calc3.variance}}</div>\r\n        <div *ngFor=\"let mes of tabMesure2500\">\r\n          <span class=\"form-control\">{{mes.index}} : {{mes.tps}}</span>\r\n        </div>\r\n        <hr>\r\n    </div>\r\n  </section>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".widget-user-header\r\n{\r\n    height : auto;\r\n}\r\n\r\n.lien\r\n{\r\n    color :white;\r\n}\r\n\r\n.time{\r\n    color: grey;\r\n    font-size: 10px;\r\n}\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\">\r\n    <section class=\"content-header\">\r\n        <h1>\r\n            {{title}} <br>\r\n          <small>Vous etes connectés : {{id}} </small>\r\n          <br>\r\n          <small>Migration  vers Java 8 : 1 octobre</small>\r\n        </h1>\r\n    </section>\r\n\r\n  <section class=\"content\">\r\n\r\n    <div class=\"row\">\r\n      <div class=\"col-md-1\"></div>\r\n      <div class=\"col-md-10\">\r\n        <!-- Box Comment -->\r\n        <div class=\"box box-widget\">\r\n          <div class=\"box-footer\">\r\n              <div  style=\"display: inline-flex;width: 100%\">\r\n                <input type=\"text\" #msg=\"ngModel\"  class=\"form-control\" name=\"msg\" [(ngModel)]=\"message.msg\" placeholder=\"Press enter to post comment\">\r\n                <button class=\"btn btn-twitter\"  [disabled]=\"!desactiver\" (click)=\"writeTwett()\" ><span class=\"fa fa-twitter\"></span></button>\r\n              </div>\r\n              <div class=\"box-body\">\r\n                <span>Timeline recup en {{getTimeline}} ms pour {{nbMessage}} tweets</span><br/>\r\n                <span *ngIf=\"timeMs\" class=\"description\">Tweet poste en : {{timeMs}} ms</span>\r\n              </div>\r\n          </div>\r\n          <!-- /.box-body -->\r\n\r\n          <!-- /.box-footer -->\r\n          <!-- /.box-footer -->\r\n        </div>\r\n        <!-- /.box -->\r\n      </div>\r\n      <!-- /.col -->\r\n      <!-- /.col -->\r\n    </div>\r\n    <!-- /.row -->\r\n\r\n    <div *ngFor=\"let msg of lesMessages\" class=\"row\">\r\n      <div class=\"col-md-1\"></div>\r\n      <div class=\"col-md-10\">\r\n        <!-- Box Comment -->\r\n        <div class=\"box box-widget\">\r\n\r\n          <div class=\"box-header with-border\">\r\n            <div class=\"user-block\">\r\n              <img class=\"img-circle\" src=\"./assets/dist/img/avatar5.png\" alt=\"User Image\">\r\n              <span *ngIf=\"msg\" class=\"username\"><a href=\"#\">{{msg.message.userId}}</a></span>\r\n              <span *ngIf=\"msg\" class=\"username time\">{{msg.timestamp | date:'yyyy-MM-dd HH:mm'}}</span>\r\n            </div>\r\n            <!-- /.user-block -->\r\n\r\n          </div>\r\n          <!-- /.box-header -->\r\n          <div class=\"box-body\">\r\n\r\n            <p>{{msg.message.message}}</p>\r\n          </div>\r\n        </div>\r\n        <!-- /.box -->\r\n      </div>\r\n    </div>\r\n    <div class=\"col-md-2\"></div>\r\n    <div class=\"col-md-6\">\r\n        <button class=\"btn center-block btn-twitter\" (click)=\"loadMoreTwett()\" ><span class=\"fa fa-twitter\">More tweets</span></button>\r\n    </div>\r\n  </section>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/dashboard/dashboard.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export msg */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_User_user_services__ = __webpack_require__("../../../../../src/app/services/User/user.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_Message_message_model__ = __webpack_require__("../../../../../src/app/models/Message/message.model.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_Twitts_twitts_service__ = __webpack_require__("../../../../../src/app/services/Twitts/twitts.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var msg = (function () {
    function msg() {
    }
    return msg;
}());

var DashboardComponent = (function () {
    function DashboardComponent(userS, twitS, route, dateP) {
        this.userS = userS;
        this.twitS = twitS;
        this.route = route;
        this.dateP = dateP;
        this.title = "Timeline";
        this.lesMessages = [];
        this.id = 4843829756690432;
        this.desactiver = true;
        this.getTimeline = null;
        this.nbMessage = 15;
        this.min = 0;
        this.timeMs = 0;
        this.nb = 5;
        this.model = {};
        this.message = {};
        this.tweet = null;
        //this.lesMessages = Message[]
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.nbMessage = 15;
        this.min = 0;
        this.getTweets();
        var datefin = new Date();
    };
    DashboardComponent.prototype.writeTwett = function () {
        var _this = this;
        this.desactiver = false;
        if (this.message.msg) {
            var msg1 = new msg();
            this.tweet = new __WEBPACK_IMPORTED_MODULE_2__models_Message_message_model__["a" /* Message */](this.message.msg, 4843829756690432);
            this.message.msg = null;
            console.log(this.tweet);
            var dateDeb_1 = new Date().getTime();
            this.twitS.addTwitts(this.tweet).subscribe(function (complete) {
                var datefin = new Date().getTime();
                console.log(complete.json());
                var message = new msg();
                _this.lesMessages.unshift(complete.json());
                _this.timeMs = datefin - dateDeb_1;
            }, function (err) { return console.log(err); });
        }
        this.desactiver = true;
    };
    DashboardComponent.prototype.getTweets = function () {
        var _this = this;
        var datedeb = new Date().getTime();
        this.twitS.getTwittsForMe(this.id, this.nbMessage).subscribe(function (complete) {
            var datefin = new Date().getTime();
            _this.getTimeline = datefin - datedeb;
            _this.lesMessages = complete.json().items;
            console.log(_this.lesMessages);
        }, function (err) { return console.log(err); });
    };
    DashboardComponent.prototype.addFollow = function () {
        var _this = this;
        var users;
        this.userS.getListUsers().subscribe(function (complete) {
            var id = 4858852746985472;
            var _loop_1 = function (i) {
                console.log(complete.json().items[i]);
                if (id != complete.json().items[i].id) {
                    setTimeout(function () {
                        _this.userS.addFollower(complete.json().items[i].id, id).subscribe(function (complete) {
                            console.log(complete.json());
                        }, function (err) { return console.log(err); });
                    }, 10000);
                }
            };
            for (var i = 0; i < complete.json().items.length; i = i + 1) {
                _loop_1(i);
            }
        }, function (err) { return console.log(err); });
    };
    DashboardComponent.prototype.createUser = function () {
        console.log(this.model);
        var min = this.model.min;
        var max = this.model.max;
        for (var i = min; i <= max; i = i + 1) {
            var user = 'user' + i.toString();
            console.log(user);
            this.userS.createUser(user).subscribe(function (complete) {
                console.log(complete.json());
                var id = complete.json().id;
                console.log(id);
            }, function (err) { return console.log(err); });
        }
    };
    DashboardComponent.prototype.loadMoreTwett = function () {
        var _this = this;
        this.min = this.nbMessage;
        this.nbMessage += 15;
        this.twitS.getMoreTweets(this.id, this.min, this.nbMessage).subscribe(function (complete) {
            console.log(complete.json());
            for (var i = 0; i < complete.json().items.length; ++i) {
                if (complete.json().items[i].message) {
                    _this.lesMessages.push(complete.json().items[i]);
                }
            }
            //console.log(this.lesMessages);
        }, function (err) { return console.log(err); });
    };
    DashboardComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            template: __webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/dashboard/dashboard.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_User_user_services__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_3__services_Twitts_twitts_service__["a" /* TwittsService */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_5__angular_common__["d" /* DatePipe */]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<footer class=\"main-footer\" style=\"background-color:#ecf0f5\">\r\n    <div class=\"pull-right hidden-xs\">\r\n        <b>Version</b> 1.2\r\n    </div>\r\n    <strong>Copyright &copy; {{year}} <a href=\"#\"></a>.</strong>\r\n    All rights reserved.\r\n</footer>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
/**
 * Created by danielahmed on 17/04/2017.
 */

var FooterComponent = (function () {
    function FooterComponent() {
        this.year = new Date().getFullYear();
    }
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/components/footer/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());

/**
 * Created by danielahmed on 17/04/2017.
 */


/***/ }),

/***/ "../../../../../src/app/components/header/header.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/header/header.component.html":
/***/ (function(module, exports) {

module.exports = "<header class=\"main-header\">\r\n    <!-- Logo -->\r\n    <a [routerLink]=\"['/dashboard']\" class=\"logo\">\r\n        <!-- mini logo for sidebar mini 50x50 pixels -->\r\n        <span class=\"logo-mini\">TT</span>\r\n        <!-- logo for regular state and mobile devices -->\r\n        <span class=\"logo-lg\"><b>TinyTwit</b></span>\r\n    </a>\r\n    <!-- Header Navbar: style can be found in header.less -->\r\n    <nav class=\"navbar navbar-static-top\">\r\n        <!-- Sidebar toggle button-->\r\n        <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">\r\n            <span class=\"sr-only\">Toggle navigation</span>\r\n        </a>\r\n\r\n        <!--<div class=\"navbar-custom-menu\">-->\r\n            <!--<ul class=\"nav navbar-nav\">-->\r\n                <!--<li class=\"dropdown user user-menu\">-->\r\n                    <!--<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">-->\r\n                        <!--<img class=\"user-image\" *ngIf=\"!user.img\" src=\" ./assets/dist/img/user.png\">-->\r\n\r\n                    <!--</a>-->\r\n                    <!--<ul class=\"dropdown-menu\">-->\r\n                        <!--&lt;!&ndash; User image &ndash;&gt;-->\r\n                        <!--<li class=\"user-header\">-->\r\n                            <!--<img class=\"user-image\"  src=\"./assets/dist/img/user.png\">-->\r\n                            <!--<p>-->\r\n                                <!--{{user.username}}-->\r\n                            <!--</p>-->\r\n                        <!--</li>-->\r\n                        <!--&lt;!&ndash; Menu Body &ndash;&gt;-->\r\n                        <!--<li class=\"user-body\">-->\r\n                            <!--<div class=\"row\">-->\r\n\r\n                            <!--</div>-->\r\n                            <!--&lt;!&ndash; /.row &ndash;&gt;-->\r\n                        <!--</li>-->\r\n                        <!--&lt;!&ndash; Menu Footer&ndash;&gt;-->\r\n                        <!--<li class=\"user-footer\">-->\r\n                            <!--<div class=\"pull-left\">-->\r\n                                <!--<a [routerLink]=\"['/profile']\" class=\"btn btn-default btn-flat\">Profile</a>-->\r\n                            <!--</div>-->\r\n                            <!--<div class=\"pull-right\">-->\r\n                                <!--<a [routerLink]=\"['/login']\" class=\"btn btn-default btn-flat\">Sign out</a>-->\r\n                            <!--</div>-->\r\n                        <!--</li>-->\r\n                    <!--</ul>-->\r\n                <!--</li>-->\r\n            <!--</ul>-->\r\n        <!--</div>-->\r\n    </nav>\r\n</header>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/header/header.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HeaderComponent = (function () {
    function HeaderComponent(router) {
        this.router = router;
        this.user = {};
        if (localStorage.getItem('user')) {
            this.user = JSON.parse(localStorage.getItem('user'));
        }
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            selector: 'app-header',
            template: __webpack_require__("../../../../../src/app/components/header/header.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/nav/sidebar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "aside.main-sidebar{\r\n    background-color: transparent;\r\n    min-height:100%;\r\n    margin: 0 auto;\r\n    border-right: solid 1px #cfcfcf;\r\n}\r\n\r\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/nav/sidebar.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- Left side column. contains the logo and sidebar -->\r\n<aside class=\"main-sidebar\">\r\n    <!-- sidebar: style can be found in sidebar.less -->\r\n    <section class=\"sidebar\">\r\n        <!-- Sidebar user panel -->\r\n        <div class=\"user-panel\">\r\n            <div class=\"pull-left image\">\r\n                <img class=\"img-circle\" src=\"./assets/dist/img/user.png\">\r\n            </div>\r\n            <div class=\"pull-left info\">\r\n                <p *ngIf=\"user\">{{ user.username }}</p>\r\n                <a href=\"#\"><i class=\"fa fa-circle text-success\"></i> Online</a>\r\n            </div>\r\n        </div>\r\n        <!-- search form -->\r\n        <!-- /.search form -->\r\n        <!-- sidebar menu: : style can be found in sidebar.less -->\r\n        <ul class=\"sidebar-menu\">\r\n            <li class=\"header\">Menu</li>\r\n            <li>\r\n                <a [routerLink]=\"['/']\">\r\n                    <i class=\"fa fa-twitter-square\"></i>\r\n                    <span>Les Twitts</span>\r\n\r\n                </a>\r\n            </li>\r\n            <li>\r\n                <a [routerLink]=\"['/userstofollow']\">\r\n                    <i class=\"fa fa-twitter\"></i>\r\n                    <span>Users to follow</span>\r\n                </a>\r\n            </li>\r\n            <li>\r\n             <a [routerLink]=\"['calcul1']\">\r\n               <i class=\"fa fa-area-chart\"></i>\r\n               <span>Calcul Follows (not working)</span>\r\n             </a>\r\n            </li>\r\n            <li>\r\n              <a [routerLink]=\"['/testMessages']\">\r\n                <i class=\"fa fa-comments-o\"></i>\r\n                <span>Test Messages (not working)</span>\r\n              </a>\r\n            </li>\r\n            <li>\r\n              <a [routerLink]=\"['/readme']\">\r\n                <i class=\"fa fa-file\"></i>\r\n                <span>Read me</span>\r\n              </a>\r\n            </li>\r\n            <li>\r\n              <a target=\"_blank\" href=\"https://github.com/demeph/WebAndCloud_projet\">\r\n                <i class=\"fa fa-github-square\"></i>\r\n                <span>Github</span>\r\n\r\n              </a>\r\n            </li>\r\n\r\n        </ul>\r\n\r\n    </section>\r\n</aside>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/nav/sidebar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SidebarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidebarComponent = (function () {
    function SidebarComponent() {
        this.user = {};
    }
    SidebarComponent.prototype.ngOnInit = function () {
        if (localStorage.getItem('user')) {
            this.user = JSON.parse(localStorage.getItem('user'));
        }
    };
    SidebarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            selector: 'sidebar',
            template: __webpack_require__("../../../../../src/app/components/nav/sidebar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/nav/sidebar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SidebarComponent);
    return SidebarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/readme/home.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\" style=\"background-color: white\">\r\n  <section class=\"content\" >\r\n  <h1>Projet Web &amp; Cloud</h1>\r\n<p>Ceci est le dépot github associé à notre projet de web and cloud.</p>\r\n<p>Jehanno Clément<br/>\r\n  Phalavandishvili Demetre <br/>\r\n  Duclos Romain<br/>\r\n  Mahier Loïc<br/></p>\r\n<p>M1-ALMA 2017-2018<br/></p>\r\n<h1>Plan</h1>\r\n<ul>\r\n  <li><p><a href='#presentation'>Présentation du sujet</a></p>\r\n  </li>\r\n  <li><p><a href='#details'>Détails d&#39;implémentation</a></p>\r\n    <ul>\r\n      <li><a href='#user'>User</a></li>\r\n      <li><a href='#message'>Message</a></li>\r\n      <li><a href='#messageIndex'>MessageIndex</a></li>\r\n\r\n    </ul>\r\n  </li>\r\n  <li><p><a href='#Tests'>Tests</a></p>\r\n    <ul>\r\n      <li><a href='#post_twitt'>Post d&#39;un tweet</a></li>\r\n      <li><a href='#recup_tweet'>Récupération des tweets</a></li>\r\n\r\n    </ul>\r\n  </li>\r\n  <li><p><a href='#scale'>Scalability</a></p>\r\n  </li>\r\n  <li><p><a href='#conclusion'>Conclusion</a></p>\r\n  </li>\r\n\r\n</ul>\r\n<h2><a name=\"presentation\"></a>Présentation du sujet</h2>\r\n<p>\r\n  <ul>\r\n    <li>Le sujet est disponible <a href='https://docs.google.com/document/d/1wVf1dWzbmxp5wtJd_I9kAHpke29FpPqe8mPOCv3J1mM/edit#'>ici</a></li>\r\n    <li>Lien de l&#39;API : <a href='https://apis-explorer.appspot.com/apis-explorer/?base=https://1-dot-webcloud-122127.appspot.com/_ah/api#p/' target='_blank' >https://apis-explorer.appspot.com/apis-explorer/?base=https://1-dot-webcloud-122127.appspot.com/_ah/api#p/</a></li>\r\n    <li>Lien de l&#39;application web : <a href='http://1-dot-webcloud-122127.appspot.com/' target='_blank' >http://1-dot-webcloud-122127.appspot.com/</a></li>\r\n    <li>La partie Front du projet est réalisé en Angular 2</li>\r\n  </ul>\r\n<h2><a name=\"details\"></a> Détails d&#39;implémentation</h2>\r\n<p>Dans cette section, nous allons expliquer certaines de nos classes ainsi que quelques décisions prises ayant influencées la construction du code.</p>\r\n<h3><a name=\"user\"></a> La classe User</h3>\r\n<p>Avant d&#39;aller plus loin, il faut que nous expliquions comment la classe User est définie.</p>\r\n<code>@PersistenceCapable(identityType=IdentityType.APPLICATION)<br/>\r\npublic class User<br/>\r\n\t@PrimaryKey<br/>\r\n\t@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY)<br/>\r\n\tKey key;<br/>\r\n\t@Persistent<br/>\r\n\tString name;<br/>\r\n\t@Persistent<br/>\r\n\t@Element(dependent=&quot;true&quot;)<br/>\r\n\tSet&lt;Long&gt; lesGensQuiMeSuit = new HashSet&lt;Long&gt;();// les gens qui me follow<br/>\r\n\t@Persistent<br/>\r\n\t@Element(dependent=&quot;true&quot;)<br/>\r\n  Set&lt;Long&gt; lesGensQueJeSuit = new HashSet&lt;Long&gt;();<br/>\r\n</code>\r\n<p>La classe User va contenir un nom d&#39;utilisateur et une clé pour l&#39;identifier de manière unique. <br/>\r\n  Dans l&#39;idée, nous pourrions aussi rajouter une photo de profil, une description, etc. Tout ce qui est relatif à l&#39;utilisateur en lui-même.<br/>\r\n  Les champs &quot;lesGensQueJeSuit&quot; et &quot;lesGensQuiMeSuit&quot; sont importants. Au premier abord, nous pourrions penser que le seul fait de suivre quelqu&#39;un est suffisant. Ainsi, nous aurions juste un champ &quot;lesGensQueJeSuit&quot;. Se pose ensuite la question : &quot;A qui dois-je envoyer un messsage quand je poste ?&quot;. <br/>\r\n  La première idée implique que nous parcourions <strong>tous</strong> les utilisateurs de la base pour aller regarder dans leur liste de follow si il faut envoyer à cet utilisateur ou non. Sur l&#39;exemple de twitter, Rihanna a 84M d&#39;abonnés, twitter recense 330M d&#39;utilisateurs aux dernières nouvelles. <br/>\r\n  Avec notre implémentation nous estimons qu&#39;il vaut mieux avoir une liste de 84M d&#39;abonnés que d&#39;en parcourir 330M a <strong>chaque</strong> fois. Ainsi pour une personne qui n&#39;a &quot;que&quot; 1M de follower, nous allons parcourir 1M de follower au lieu de 330M. Encore une fois, il s&#39;agit de perdre en taille pour gagner en temps d’exécution. <br/></p>\r\n<h3><a name=\"message\"></a> La classe Message</h3>\r\n<p>Voici comment nous avons organisé notre classe Message :</p>\r\n<code>public class Message<br/>\r\n @PrimaryKey<br/>\r\n    @Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY)<br/>\r\n    private Key msgId;<br/>\r\n    @Persistent<br/>\r\n    private Long userId;<br/>\r\n    @Persistent<br/>\r\n    private String message;<br/>\r\n</code>\r\n<p>Fondamentalement, un message va contenir une clé comme identifiant unique ainsi que l&#39;id du User qui à posté ce message.\r\n  La classe Message ne sert qu&#39;à envoyer du texte, nous pourrions très bien y ajouter des photos, des gifs, des vidéos, etc.\r\n  Cependant, ce qui nous importe est ce que nous avons vu dans la classe User : lorsqu&#39;un utilisateur poste un message, il faut savoir à qui l&#39;envoyer. Hors, l&#39;information n&#39;est pas définie ici. Nous avons pour y remedier créé une classe MessageIndex.</p>\r\n<h3><a name=\"messageIndex\"></a> La classe Message Index</h3>\r\n<code>@PersistenceCapable(identityType=IdentityType.APPLICATION)<br/>\r\npublic class MessageIndex<br/>\r\n\t@PrimaryKey<br/>\r\n\t@Persistent(valueStrategy=IdGeneratorStrategy.IDENTITY)<br/>\r\n\tKey k;<br/>\r\n\t@Persistent<br/>\r\n\tMessage msg;<br/>\r\n\t@Persistent<br/>\r\n\tLong userId;<br/>\r\n\t@Persistent<br/>\r\n\tlong timestamp;<br/>\r\n\t@Persistent<br/>\r\n\tSet&lt;Long&gt; receivers = new HashSet&lt;Long&gt;();<br/>\r\n</code>\r\n<p>La classe messageIndex est primordiale pour passer à l&#39;échelle. <br/>\r\n  Nous avons vu en cours que nos requêtes doivent être rapides, pour ce faire il ne faut pas déseraliser et sérialiser l&#39;intégralité du message (body, photos, etc.). Si vous êtes novices vous trouverez des informations complémentaires <a href='https://www.youtube.com/watch?v=AgaL6NGpkB8'>ici</a>. <br/>\r\n  C&#39;est pourquoi nous avons la classe MessageIndex, avec quelques petits details supplémentaires :\r\n  La clé est l&#39;identificateur unique. Le fait que le message soit présent ici peut surprendre. En fait, la relation de &quot;parenté&quot; dans le cloud définie par les Entity Group (comme expliqué dans la vidéo plus haut) est gérée automatiquement par le JDO de cette manière. Une autre option consistait à définir deux entitées Message et MessageIndex puis à les push en spécifiant que Message est le parent de MessageIndex. Cependant, le JDO le fait et c&#39;est plus simple. <br/></p>\r\n<p>Le userId ici est plus qu&#39;étonnant car il faudrait le laisser dans la classe message. Cependant, nous avons un problème quand il faut gérer la rétroactivité des messages. Si un utilisateur en follow un autre, il faut qu&#39;il puisse voir les anciens tweet de la personne qu&#39;il vient de follow. Pour faire cela, il faut parcourir tous les messages, trouver l&#39;id de la bonne personne (afin de savoir quels sont ses messages) et ajouter à la liste des receiver la personne qui vient de le suivre. Or, si nous laissons le userID dans le message, il va falloir le désérialiser entièrement (y compris le body). Mais nous perdons alors trop de temps, nous avons donc décidé de le descendre dans messageIndex. <br/></p>\r\n<p>Le timestamp nous permet juste de faire les requêtes demandées. Quand on nous demande d&#39;afficher les 10 derniers messages, il faut pouvoir identifier l&#39;ordre de parution des messages. Nous rajoutons donc un Timestamp qui est instancié à la création du message. <br/>\r\n  Les receiver sont les personnes à qui sont destiné le message, c&#39;est pour cela que nous avons besoin de la liste de nos follower dans User. Donc nous nous basons sur cette liste, puis nous nous rajoutons afin de voir nos propores tweets. <br/>\r\n  Un autre petit détail : quand nous parlons du timestamp, nous ommettons de dire que cela ne fonctionne pas comme ça. Pour savoir quels sont les 10 derniers messages d&#39;un utilisateur, il faut bien 2 choses : les 10 <strong>derniers</strong> destinés à <strong>l&#39;utilisateur</strong>. Pour faire cette requête, nous avons besoin d&#39;un index multivalué sur ces deux champs. <br/></p>\r\n<h2><a name=\"Tests\"></a> Tests</h2>\r\n<p>Désormais il s&#39;agit de mesurer nos temps d&#39;exécution.</p>\r\n<p>Avant toute chose, il était demandé de faire des tests dans des configurations maximales de 5000 followers/abonnements. Hors, afin de pouvoir tester correctement, et au vu des quotas assignés par Google, nous ne pourrons tester que pour des configurations maximales de 2500 personnes. </p>\r\n<p>Pour rappel, il est attendu que nous mesurions :</p>\r\n<p>_ Pour une personne <strong>suivie par</strong> 100, 1000 et 2500 personnes le temps d&#39;exécution pour poster, en moyenne, 10 tweets.</p>\r\n<p>_ Pour une personne <strong>qui suit</strong> 100, 1000 et 2500 personnes le temps d&#39;exécution moyen pour récupérer ses 10, 50 et 100 derniers messages.</p>\r\n<p>Une interface dynamique pour ces tests est disponible sur l&#39;application web dans l&#39;onglet <a href='http://1-dot-webcloud-122127.appspot.com/calcul1'>Calcul Follow</a> et <a href='http://1-dot-webcloud-122127.appspot.com/testMessages'>Test Messages</a>. </p>\r\n<p>Aussi avant d&#39;aller plus loin, il faut revenir sur un point important : au niveau des temps d&#39;exécutions, les premiers appels sont complètements faussés (démarrage des VM, premier accès plus lent, etc). Avec la monté en charge (de plus en plus de requête), nos résultats sont plus cohérents mais apparraissent aussi des anomalies.</p>\r\n<p>Nous avons donc enlevé ces résultats qui s&#39;éloignent vraiment trop pour nos calculs de moyennes, variances et écart-type.</p>\r\n<h3><a name=\"post_twitt\" ></a>Post d&#39;un tweet</h3>\r\n<p>Nous avons posté pour chaque configuration 15 messages et nous avons récupéré le temps que cela prenait pour chaque message :</p>\r\n<p><img src='https://raw.githubusercontent.com/loic44650/WebAndCloud_projet/master/srcImg/calcul%20post%20tweet.png' alt='Image pour récupérer les 15 derniers messages suivant la configuration' /></p>\r\n<p>Résultats :</p>\r\n<figure><table>\r\n  <thead>\r\n  <tr><th style='text-align:center;' >Nombre de followers</th><th style='text-align:center;' >100</th><th style='text-align:center;' >1000</th><th style='text-align:center;' >2500</th></tr></thead>\r\n  <tbody><tr><td style='text-align:center;' >Moyenne (en ms)</td><td style='text-align:center;' >286.2</td><td style='text-align:center;' >326</td><td style='text-align:center;' >795,64286</td></tr><tr><td style='text-align:center;' >Variance</td><td style='text-align:center;' >2942,1598492224</td><td style='text-align:center;' >3190,60041316</td><td style='text-align:center;' >16989,0864939961</td></tr><tr><td style='text-align:center;' >Ecart type (en ms)</td><td style='text-align:center;' >54,24168</td><td style='text-align:center;' >56,4854</td><td style='text-align:center;' >130,34219</td></tr></tbody>\r\n</table></figure>\r\n<p>&nbsp;</p>\r\n<h3><a name=\"recup_tweet\" ></a> Récupération des tweet</h3>\r\n<p>Pour remplir cette section, nous avons écrit 1 message par utilisateur dans la base.</p>\r\n<p>Il faut donc suivant les configurations de follower, récupérer les 10, 50, et 100 derniers messages.</p>\r\n<p>Les tests ne seront effectués que sur 15 mesures pour ne pas atteindre les quotas trop rapidement.</p>\r\n<h6>Récupérer 10 tweet en fonction du nombre d&#39;abonnements</h6>\r\n<p><img width=\"80%\" src='https://raw.githubusercontent.com/loic44650/WebAndCloud_projet/master/srcImg/Calcul%20recup%2010%20messages.png' alt='Lien de l&#39;image test 10 derniers messages' /></p>\r\n<figure><table>\r\n  <thead>\r\n  <tr><th style='text-align:center;' >Nombre d&#39;abonnements</th><th style='text-align:center;' >100</th><th style='text-align:center;' >1000</th><th style='text-align:center;' >2500</th></tr></thead>\r\n  <tbody><tr><td style='text-align:center;' >Moyenne (en ms)</td><td style='text-align:center;' >508,06667</td><td style='text-align:center;' >582,16667</td><td style='text-align:center;' >781</td></tr><tr><td style='text-align:center;' >Variance (en ms)</td><td style='text-align:center;' >28559,9285500489</td><td style='text-align:center;' >25732,3049029264</td><td style='text-align:center;' >29753,8660977481</td></tr><tr><td style='text-align:center;' >Ecart type (en ms)</td><td style='text-align:center;' >168,99683</td><td style='text-align:center;' >160,41292</td><td style='text-align:center;' >172,49309</td></tr></tbody>\r\n</table></figure>\r\n<h6>Récupérer 50 tweet en fonction du nombre d&#39;abonnements</h6>\r\n<p><img  width=\"80%\" src='https://raw.githubusercontent.com/loic44650/WebAndCloud_projet/master/srcImg/Calcul%2050%20messages.png' alt='Lien de l&#39;image test 50 derniers messages' /></p>\r\n<figure><table>\r\n  <thead>\r\n  <tr><th style='text-align:center;' >Nombre d&#39;abonnements</th><th style='text-align:center;' >100</th><th style='text-align:center;' >1000</th><th style='text-align:center;' >2500</th></tr></thead>\r\n  <tbody><tr><td style='text-align:center;' >Moyenne (en ms)</td><td style='text-align:center;' >925</td><td style='text-align:center;' >699.7</td><td style='text-align:center;' >712,42857</td></tr><tr><td style='text-align:center;' >Variance (en ms)</td><td style='text-align:center;' >54094,9216639225</td><td style='text-align:center;' >79206,2108385604</td><td style='text-align:center;' >61489,81521796</td></tr><tr><td style='text-align:center;' >Ecart type (en ms)</td><td style='text-align:center;' >232,58315</td><td style='text-align:center;' >281,43598</td><td style='text-align:center;' >247,9714</td></tr></tbody>\r\n</table></figure>\r\n<h6>Récupérer 100 tweet en fonction du nombre d&#39;abonnements</h6>\r\n<p><img  width=\"80%\"src='https://raw.githubusercontent.com/loic44650/WebAndCloud_projet/master/srcImg/Calcul%20100%20messages.png' alt='Lien de l&#39;image test 100 derniers messages' /></p>\r\n<figure><table>\r\n  <thead>\r\n  <tr><th style='text-align:center;' >Nombre d&#39;abonnements</th><th style='text-align:center;' >100</th><th style='text-align:center;' >1000</th><th style='text-align:center;' >2500</th></tr></thead>\r\n  <tbody><tr><td style='text-align:center;' >Moyenne (en ms)</td><td style='text-align:center;' >1461,57143</td><td style='text-align:center;' >1921,36364</td><td style='text-align:center;' >2172,55556</td></tr><tr><td style='text-align:center;' >Variance (en ms)</td><td style='text-align:center;' >242634,6721877521</td><td style='text-align:center;' >183378,7746271504</td><td style='text-align:center;' >204808,2455505025</td></tr><tr><td style='text-align:center;' >Ecart type (en ms)</td><td style='text-align:center;' >492,57961</td><td style='text-align:center;' >428,22748</td><td style='text-align:center;' >452,55745</td></tr></tbody>\r\n</table></figure>\r\n<h2><a name=\"scale\"></a> Scalability</h2>\r\n<h4>Résumé des performances <br/></h4>\r\n<p>Tout d&#39;abord, il faut noter que nos résultats ne sont que des échantillons et ne sont pas forcément représentatifs. Mais ils suffisent pour exhiber notre scalabilité. </p>\r\n<p>Aussi, nous avons l&#39;impression que Google bloque parfois les requêtes ou les ralentits fortement. Les serveurs sont également indisponibles de temps en temps. Cela nous semble légitime, nous utilisons un services gratuit ce qui ne nous assure pas d&#39;un fonctionnement 100% optimal.</p>\r\n<p>Résumé des performances pour poster un tweet en fonction des gens qui nous suivent :</p>\r\n<p><img width=\"70%\" src='https://raw.githubusercontent.com/loic44650/WebAndCloud_projet/master/srcImg/graphe2.png' alt='Résumé des performances pour poster un tweet en fonction des gens qui nous suivent' /></p>\r\n<p>Nous observons que suivant le nombre de personnes qui nous suivent, les temps augmentent rapidement mais reste raisonnables : moins de 1 sec en moyenne pour tweet à 2500 personnes. A priori les temps semblent augmenter, mais même à une échelle supérieur, la scalability fera que la diférence ne sera pas si grande.</p>\r\n<p>Résumé des performances pour la récupération des tweet :</p>\r\n<p><img width=\"70%\" src='https://raw.githubusercontent.com/loic44650/WebAndCloud_projet/master/srcImg/graphe.png' alt='résumé des performances pour la récupération des tweets ' /></p>\r\n<p>Les résultats de performance que nous obtenons à première vu semblent un peu long, mais en réalité, récupérer 100 tweet avec 2500 abonnements en 2 secondes reste relativement correct. Etant donné qu&#39;il n&#39;y a pas un écart énorme entre une récupération de 100 tweet avec 100, 1000 et 2500 abonnements, nous pouvons dire que l&#39;application passe à l&#39;échelle. Du fait de nos quotas limités, il est difficile de mettre en avant des résultats très satisfaisants. En effet nous n&#39;avons pas pu effectuer autant de test que l&#39;aurions souhaité. Mais nous avons réussi à obtenir des temps de réponse bien meilleur que ceux mesurés dans le rapport.</p>\r\n<p>Finalement, la question est :</p>\r\n<h4>Est-ce que cela scale ? <br/></h4>\r\n<p>D&#39;après les graphiques qui résument les performances de l&#39;API : oui.</p>\r\n<p>Nos messages vont scale grâce à MessageIndex, en fait c&#39;est tout l&#39;intérêt de cette classe. Dans les faits, il est possible de faire pareil pour nos user, mais ici ce n&#39;est pas utile car notre utilisateur n&#39;a &quot;que&quot; un nom. En effet, si il avait une photo de profil, une description, une bannière, etc,il serait <del>intéressant</del> nécessaire de faire la même chose pour scale correctement.</p>\r\n<h2><a name=\"conclusion\"></a> Conclusion</h2>\r\n<p>Au final, nous pensons avoir deployé une application web qui passe à l&#39;échelle. Nous avons eu quelques difficultés techniques tout au long du projet mais l&#39;épreuve principale était de faire fonctionner nos services pour des milliers d&#39;utilisateurs. Nous avons pu remarquer à quel point vouloir faire une application qui passe à l&#39;échelle peut couter cher pour un datastore (quota rempli en quelques tests). Cela nous donne aussi une idée de la complexité et du coup des applications comme Twitter ou Facebook qui doivent gérer des millions de données.</p>\r\n  </section>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/readme/readme.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ReadmeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ReadmeComponent = (function () {
    function ReadmeComponent() {
    }
    ReadmeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            template: __webpack_require__("../../../../../src/app/components/readme/home.html"),
        })
    ], ReadmeComponent);
    return ReadmeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/testMessage/testMessage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return testComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_Twitts_twitts_service__ = __webpack_require__("../../../../../src/app/services/Twitts/twitts.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_User_user_services__ = __webpack_require__("../../../../../src/app/services/User/user.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__CalculFollowers_calculFollowers_compotent__ = __webpack_require__("../../../../../src/app/components/CalculFollowers/calculFollowers.compotent.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__interfaces_calculMath_calculMath__ = __webpack_require__("../../../../../src/app/interfaces/calculMath/calculMath.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var testComponent = (function () {
    function testComponent(msgService, userS) {
        this.msgService = msgService;
        this.userS = userS;
        this.user100 = 5325894738509824;
        this.user1000 = 4688427370938368;
        this.user2500 = 5236715648188416;
        this.tabMesure100 = [];
        this.tabMesure1000 = [];
        this.tabMesure2500 = [];
        this.title = "Test de recupere les tweets";
        this.bool3 = false;
        this.bool2 = false;
        this.bool1 = false;
        this.ajoutMessage = false;
    }
    testComponent.prototype.ngOnInit = function () {
        this.bool1 = this.bool2 = this.bool3 = false;
        this.calc1 = this.calc2 = this.calc3 = null;
    };
    testComponent.prototype.getTimelineUser100 = function (val, nbMesure) {
        var _this = this;
        if (this.ajoutMessage) {
            this.tabMesure100 = [];
            var _loop_1 = function (i) {
                var dateDeb = new Date().getTime();
                this_1.msgService.getTwittsForMe(this_1.user100, val).subscribe(function (complete) {
                    var dateFin = new Date().getTime();
                    _this.tabMesure100.push(new __WEBPACK_IMPORTED_MODULE_3__CalculFollowers_calculFollowers_compotent__["b" /* Mesure */](i + 1, (dateFin - dateDeb)));
                }, function (err) { return console.log(err); });
            };
            var this_1 = this;
            for (var i = 0; i < nbMesure; i = i + 1) {
                _loop_1(i);
            }
            this.bool1 = true;
            setTimeout(function () {
                _this.calc1 = new __WEBPACK_IMPORTED_MODULE_4__interfaces_calculMath_calculMath__["a" /* calculMath */]();
                _this.calc1.moyenne = _this.calc1.calculAvg(_this.tabMesure100);
                _this.calc1.variance = _this.calc1.calculVar(_this.tabMesure100);
            }, 30000);
        }
    };
    testComponent.prototype.getTimelineUser1000 = function (val, nbMesure) {
        var _this = this;
        if (this.ajoutMessage) {
            this.tabMesure1000 = [];
            var _loop_2 = function (i) {
                var dateDeb = new Date().getTime();
                this_2.msgService.getTwittsForMe(this_2.user1000, val).subscribe(function (complete) {
                    var dateFin = new Date().getTime();
                    _this.tabMesure1000.push(new __WEBPACK_IMPORTED_MODULE_3__CalculFollowers_calculFollowers_compotent__["b" /* Mesure */](i + 1, (dateFin - dateDeb)));
                }, function (err) { return console.log(err); });
            };
            var this_2 = this;
            for (var i = 0; i < nbMesure; i = i + 1) {
                _loop_2(i);
            }
            this.bool2 = true;
            setTimeout(function () {
                _this.calc2 = new __WEBPACK_IMPORTED_MODULE_4__interfaces_calculMath_calculMath__["a" /* calculMath */]();
                _this.calc2.moyenne = _this.calc2.calculAvg(_this.tabMesure1000);
                _this.calc2.variance = _this.calc2.calculVar(_this.tabMesure1000);
            }, 30000);
        }
    };
    testComponent.prototype.calculMoyenne = function () {
    };
    testComponent.prototype.getTimelineUser2500 = function (val, nbMesure) {
        var _this = this;
        if (this.ajoutMessage) {
            this.tabMesure2500 = [];
            var _loop_3 = function (i) {
                var dateDeb = new Date().getTime();
                this_3.msgService.getTwittsForMe(this_3.user2500, val).subscribe(function (complete) {
                    var dateFin = new Date().getTime();
                    _this.tabMesure2500.push(new __WEBPACK_IMPORTED_MODULE_3__CalculFollowers_calculFollowers_compotent__["b" /* Mesure */](i + 1, (dateFin - dateDeb)));
                }, function (err) { return console.log(err); });
            };
            var this_3 = this;
            for (var i = 0; i < nbMesure; i = i + 1) {
                _loop_3(i);
            }
            this.bool3 = true;
            setTimeout(function () {
                _this.calc3 = new __WEBPACK_IMPORTED_MODULE_4__interfaces_calculMath_calculMath__["a" /* calculMath */]();
                _this.calc3.moyenne = _this.calc3.calculAvg(_this.tabMesure2500);
                _this.calc3.variance = _this.calc3.calculVar(_this.tabMesure2500);
            }, 30000);
        }
    };
    testComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            template: __webpack_require__("../../../../../src/app/components/testMessage/testMessage.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_Twitts_twitts_service__["a" /* TwittsService */],
            __WEBPACK_IMPORTED_MODULE_2__services_User_user_services__["a" /* UserService */]])
    ], testComponent);
    return testComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/testMessage/testMessage.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\" style=\"background-color: white\">\r\n  <!-- Content Header (Page header) -->\r\n  <section class=\"content-header\">\r\n    <h1>\r\n      {{title}}\r\n    </h1>\r\n  </section>\r\n\r\n  <section class=\"content\" style=\"margin: 0 auto 0 auto;text-align: center\">\r\n    <!--<div class=\"col-md-12\">-->\r\n      <!--<button  (click)=\"ecrireDesTweets()\" class=\"btn btn-twitter\">Ecrire des messages</button>-->\r\n    <!--</div>-->\r\n    <div class=\"col-md-12\" style=\"margin: 0 0 0 3px\">\r\n      Combien de messages a recupere :<br/>\r\n      <input #vall1 type=\"number\" class=\"form-control\">\r\n      Nombre de mesure a recupere :<br/>\r\n      <input #val1 type=\"number\" class=\"form-control\">\r\n      <button (click)=\"getTimelineUser100(vall1.value,val1.value)\" style=\"margin: 3px 0 0 15px\" class=\"btn btn-danger\">Recuperer des Messages pour 100 abonnements</button>\r\n      <br/>\r\n      <div *ngIf=\"bool1\">La moyenne et la variance sont calcules dans 25 secondes</div>\r\n      <div style=\"margin: 2px\" *ngIf=\"calc1\">Moyenne : {{calc1.moyenne}}, variance : {{calc1.variance}}</div>\r\n      <div *ngFor=\"let mes of tabMesure100\">\r\n        <span class=\"form-control\">{{mes.index}} : {{mes.tps}}</span>\r\n      </div>\r\n      <hr>\r\n    </div>\r\n    <div class=\"col-md-12\" style=\"margin: 0 0 0 3px\">\r\n      Combien de messages a recupere :<br/>\r\n      <input #vall2 type=\"number\" class=\"form-control\">\r\n      Nombre de mesure a recupere :<br/>\r\n      <input #val2 type=\"number\" class=\"form-control\">\r\n      <button (click)=\"getTimelineUser1000(vall2.value,val2.value)\" style=\"margin:3px 0 0 15px\" class=\"btn btn-danger\">Recuperer des Messages pour 1000 abonnements</button>\r\n      <br/>\r\n      <div *ngIf=\"bool2\">La moyenne et la variance sont calcules dans 25 secondes</div>\r\n      <span style=\"margin: 2px\" *ngIf=\"calc2\">Moyenne : {{calc2.moyenne}}, variance : {{calc2.variance}}</span>\r\n      <div *ngFor=\"let mes of tabMesure1000\">\r\n        <span class=\"form-control\">{{mes.index}} : {{mes.tps}}</span>\r\n      </div>\r\n      <hr>\r\n    </div>\r\n    <div class=\"col-md-12\" style=\"margin: 0 0 0 3px\">\r\n      Combien de messages a recupere :<br/>\r\n      <input #vall3 type=\"number\" class=\"form-control\">\r\n      Nombre de mesure a recupere :<br/>\r\n      <input #val3 type=\"number\" class=\"form-control\">\r\n      <button (click)=\"getTimelineUser2500(vall3.value,val3.value)\" style=\"margin: 3px 0 0 15px\" class=\"btn btn-danger\">Recuperer des Messages pour 2500 abonnements</button>\r\n      <br/>\r\n      <div *ngIf=\"bool3\">La moyenne et la variance sont calcules dans 25 secondes</div>\r\n      <span style=\"margin: 2px\" *ngIf=\"calc3\">Moyenne : {{calc3.moyenne}}, variance : {{calc3.variance}}</span>\r\n      <div *ngFor=\"let mes of tabMesure2500\">\r\n        <span class=\"form-control\">{{mes.index}} : {{mes.tps}}</span>\r\n      </div>\r\n      <hr>\r\n    </div>\r\n  </section>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/components/userstofofollow/userstofollow.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return usersToFollowComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_User_user_services__ = __webpack_require__("../../../../../src/app/services/User/user.services.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var usersToFollowComponent = (function () {
    function usersToFollowComponent(userS) {
        this.userS = userS;
        this.title = "Les Personnes que vous pouvez connaitre";
        this.id = 4843829756690432;
        this.lesUsers = [];
        this.min = 0;
        this.max = 15;
        this.temps = null;
    }
    usersToFollowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.temps = null;
        this.min = 0;
        this.max = 15;
        this.userS.getSuggestions(this.id, this.min, this.max).subscribe(function (complete) {
            _this.lesUsers = complete.json().items;
        }, function (err) { return console.log(err); });
    };
    usersToFollowComponent.prototype.addfollow = function (idFol) {
        var _this = this;
        var dateDeb = new Date().getTime();
        console.log(idFol);
        this.userS.addFollower(this.id, idFol).subscribe(function (complete) {
            var dateFin = new Date().getTime();
            _this.temps = dateFin - dateDeb;
            console.log(complete.json());
        });
    };
    usersToFollowComponent.prototype.getMorePeople = function () {
        var _this = this;
        this.min = this.max + 1;
        this.max = this.max + (this.min - 1);
        this.userS.getSuggestions(this.id, this.min, this.max).subscribe(function (complete) {
            var users = complete.json().items;
            for (var i = 0; i < users.length; i = i + 1) {
                _this.lesUsers.push(users[i]);
            }
        });
    };
    usersToFollowComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
            moduleId: module.i,
            template: __webpack_require__("../../../../../src/app/components/userstofofollow/userstofollow.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_User_user_services__["a" /* UserService */]])
    ], usersToFollowComponent);
    return usersToFollowComponent;
}());



/***/ }),

/***/ "../../../../../src/app/components/userstofofollow/userstofollow.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"content-wrapper\" style=\"background-color: white\">\r\n  <!-- Content Header (Page header) -->\r\n  <section class=\"content-header\">\r\n    <h1>\r\n      {{title}}\r\n    </h1>\r\n  </section>\r\n\r\n  <section class=\"content\">\r\n    <div class=\"row\">\r\n      <div *ngIf=\"temps\"class=\"box box-widget\">\r\n        <span class=\"box-header with-border\">Suivre qqn ca prend {{temps}} ms</span>\r\n      </div>\r\n      <div *ngFor=\"let user of lesUsers\" class=\"col-md-4\">\r\n        <div class=\"box box-widget\">\r\n\r\n          <div class=\"box-header with-border\">\r\n\r\n            <img style=\"width: 50px\" class=\"img-circle\" src=\"./assets/dist/img/avatar5.png\" alt=\"User Image\">\r\n            <div style=\"display: inline-block\">\r\n              <div>\r\n                <span class=\"username\"><a href=\"#\">{{user.name}}</a></span>\r\n                <input  hidden #idfollow type=\"number\" value=\"{{user.id}}\" readonly>\r\n              </div>\r\n              <button style=\"margin-left:70px\" class=\"btn-twitter right\" (click)=\"addfollow(idfollow.value)\" >Suivre</button>\r\n            </div>\r\n\r\n            <!-- /.user-block -->\r\n          </div>\r\n        </div>\r\n        <!-- /.box -->\r\n      </div>\r\n    </div>\r\n    <button class=\"btn btn-bitbucket\" (click)=\"getMorePeople()\">Get More People</button>\r\n  </section>\r\n</div>\r\n"

/***/ }),

/***/ "../../../../../src/app/guards/authentication.guard.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationGuard; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthenticationGuard = (function () {
    function AuthenticationGuard(router) {
        this.router = router;
    }
    AuthenticationGuard.prototype.canActivate = function () {
        if (localStorage.getItem('user')) {
            return true;
        }
        // not logged in so redirect to login page
        this.router.navigate(['/login']);
        return false;
    };
    AuthenticationGuard = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]])
    ], AuthenticationGuard);
    return AuthenticationGuard;
}());



/***/ }),

/***/ "../../../../../src/app/interfaces/calculMath/calculMath.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return calculMath; });
var calculMath = (function () {
    function calculMath() {
    }
    calculMath.prototype.calculVar = function (tab) {
        var avg = 0;
        avg = this.calculAvg(tab);
        var temp = 0;
        for (var i = 0; i < tab.length; ++i) {
            var cal = (tab[i].tps - avg);
            temp += (Math.pow(cal, 2));
        }
        return (temp / (tab.length - 1));
    };
    calculMath.prototype.calculAvg = function (tab) {
        var somme = 0;
        for (var i = 0; i < tab.length; ++i) {
            somme += tab[i].tps;
        }
        return somme / tab.length;
    };
    return calculMath;
}());



/***/ }),

/***/ "../../../../../src/app/models/Message/message.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Message; });
var Message = (function () {
    function Message(message, userId) {
        this.message = message;
        this.userId = userId;
    }
    return Message;
}());



/***/ }),

/***/ "../../../../../src/app/models/User/user.model.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
var User = (function () {
    function User() {
    }
    return User;
}());



/***/ }),

/***/ "../../../../../src/app/services/Authentication/authentication.services.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__User_user_services__ = __webpack_require__("../../../../../src/app/services/User/user.services.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ng_gapi__ = __webpack_require__("../../../../ng-gapi/lib/index.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AuthenticationService = (function () {
    function AuthenticationService(router, http, userServ, googleAuth) {
        this.router = router;
        this.http = http;
        this.userServ = userServ;
        this.googleAuth = googleAuth;
        this.model = { email: 'admin@tinytwitt.fr', pass: 'toto' };
    }
    AuthenticationService.prototype.loginLocal = function (username, password) {
        console.log(this.model);
        console.log(username);
        console.log(password);
        if (username = this.model.email) {
            console.log('fuck');
            if (password = this.model.pass) {
                localStorage.setItem('user', JSON.stringify({ username: username }));
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    };
    AuthenticationService.prototype.loginWithGoogle = function () {
        this.googleAuth.getAuth().subscribe(function (auth) {
            auth.signIn().then(function (complete) {
                console.log(complete.json());
            }, function (err) { return console.log(err); }),
                function (err) { return console.log(err); };
        });
    };
    AuthenticationService.prototype.logout = function () {
        /* let currentUser = JSON.parse(localStorage.getItem('currentUser'));
         if(currentUser){
             let headers = new Headers({ 'X-Auth-Token': currentUser.token,'Access-Control-Allow-Origin': '*' });
             let options = new RequestOptions({ headers: headers });
             let id = currentUser.id;
             this.token = null;
             console.log(localStorage.length);
             localStorage.clear();
             console.log(localStorage.length);
             this.http.delete(adressBackEnd+"/auth-tokens/"+currentUser.id, options).subscribe(
                 complete => {
                     console.log(id);
                 },
                 err => console.log(err),
             );
         }*/
        localStorage.clear();
        this.router.navigate(['/login']);
    };
    AuthenticationService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_2__User_user_services__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_5_ng_gapi__["b" /* GoogleAuthService */]])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "../../../../../src/app/services/Twitts/twitts.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TwittsService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TwittsService = (function () {
    function TwittsService(http) {
        this.http = http;
        this.localurl = __WEBPACK_IMPORTED_MODULE_4__app_component__["b" /* adressBackEnd */] + 'messages';
    }
    TwittsService.prototype.addTwitts = function (twitt) {
        return this.http.post('https://webcloud-122127.appspot.com/_ah/api/messages/v1/messages', twitt);
    };
    TwittsService.prototype.getTwittsForMe = function (userId, nb) {
        return this.http.get('https://webcloud-122127.appspot.com/_ah/api/messages/v1/messagesTimeline/'
            + userId.toString() + '?nbDeMessages=' + nb.toString());
    };
    TwittsService.prototype.getMoreTweets = function (userId, min, max) {
        return this.http.get('https://webcloud-122127.appspot.com/_ah/api/messages/v1/messagesTimeline/'
            + userId.toString() + '/min/' + min.toString() + '/max/' + max.toString());
    };
    TwittsService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], TwittsService);
    return TwittsService;
}());



/***/ }),

/***/ "../../../../../src/app/services/User/user.services.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export follow */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/catch.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_User_user_model__ = __webpack_require__("../../../../../src/app/models/User/user.model.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var follow = (function () {
    function follow() {
    }
    return follow;
}());

var UserService = (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.getListUsers = function () {
        return this.http.get('https://webcloud-122127.appspot.com/_ah/api/users/v1/users');
    };
    UserService.prototype.createUser = function (name) {
        var us = new __WEBPACK_IMPORTED_MODULE_4__models_User_user_model__["a" /* User */]();
        us.name = name;
        return this.http.post('https://webcloud-122127.appspot.com/_ah/api/users/v1/users', us);
    };
    UserService.prototype.addFollower = function (myUserId, otherUserId) {
        var us = new follow();
        us.followToAdd = otherUserId;
        return this.http.post('https://webcloud-122127.appspot.com/_ah/api/users/v1/users/' + myUserId.toString()
            + '/addFollower?aSuivre=' + otherUserId.toString(), us);
    };
    //genereFollower
    UserService.prototype.getSuggestions = function (id, min, max) {
        return this.http.get('https://webcloud-122127.appspot.com/_ah/api/users/v1/users/'
            + id + '/getSuggestions/min/' + min + '/max/' + max);
    };
    UserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* enableProdMode */])();
    console.log = function () { };
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map