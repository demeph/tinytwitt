/**
 * Created by danielahmed on 17/04/2017.
 */
import { Component } from '@angular/core';

@Component({
    moduleId : module.id,
    selector: 'app-footer',
    templateUrl: 'footer.component.html',
    styleUrls: ['footer.component.css']
})
export class FooterComponent  {

  year : number

  constructor(){
      this.year = new Date().getFullYear();
  }

}
/**
 * Created by danielahmed on 17/04/2017.
 */
