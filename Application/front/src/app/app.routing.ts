import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {CalculFollowersComponent} from "./components/CalculFollowers/calculFollowers.compotent";
import {usersToFollowComponent} from "./components/userstofofollow/userstofollow.component";
import {testComponent} from "./components/testMessage/testMessage.component";
import {ReadmeComponent} from "./components/readme/readme.component";

const appRoutes : Routes = [
    {path: '', component: DashboardComponent },
    {path : 'calcul1',component:CalculFollowersComponent},
    {path : 'readme', component:ReadmeComponent},
    {path : 'userstofollow',component:usersToFollowComponent},
    {path : 'testMessages',component:testComponent},
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes,{useHash: true});
